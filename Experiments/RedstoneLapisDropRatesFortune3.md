# Redstone and Lapis Lazuli Ore Drop Rates and Fortune III’s Effect

## Introduction

To properly calculate the value of redstone dust and lapis lazuli in relation to other drops and their respective ores, we must know how many items a single ore drops on average and how Fortune III affects these rates.

## Research

According to the official Minecraft Wiki, a single redstone ore will drop 4–5 ores when mined with an unenchanted pickaxe. [[1]](http://minecraft.gamepedia.com/Redstone) It also states that, if the ore is mined with a Fortune III pickaxe, 0–3 redstone will be dropped in addition to the base average of 4–5, giving a final range of 4–8 possible pieces of redstone dropped per ore.

From the same wiki, we’re told that a lapis lazuli ore will yield anywhere from 4–8 dyes if mined with an unenchanted pickaxe. [[2]](http://minecraft.gamepedia.com/Lapis_Lazuli) Fortune III then increases this range to 4–32 pieces per ore.

## Hypothesis

We should expect that redstone will have an average drop rate of 4.5 pieces per ore for an unenchanted pickaxe and 6 pieces per ore if mined with a Fortune III-enchanted pickaxe, assuming that each integer in these ranges has an equal chance of occurring. We should also expect an average of 6 pieces of lapis lazuli yielded per ore mined with an unenchanted pickaxe and 18 with a Fortune III-enchanted pickaxe.

## Experiment

### Method

We will generate four arrays of ore in our experiment, two for each type of ore. One of the arrays will be mined with a pickaxe without a Fortune III enchantment and the other mined with a pickaxe with a Fortune III enchantment. Each array will consist of ten rows, each with sixty-four pieces of ore and a chest at the end.

We will start with the appropriate pickaxe for the array and mine all ore in a single row, then put all of the drops accumulated inside the chest at the end of each row. The process, called a ‘run’, will then be repeated for the rest of the rows in the array and that will complete a single ‘set’ of data. We will repeat this process for the other three arrays and compile the data.

Finally, each set will then be assigned the average drop rate of the rows.

### Results

#### Redstone Ore

Run                 | Total Drops (Regular Pickaxe) (Set 1) | Total Drops (Fortune III) (Set 2)
-:                  | -:                                    | -:
1                   | 291                                   | 385
2                   | 284                                   | 378
3                   | 290                                   | 359
4                   | 289                                   | 374
5                   | 291                                   | 397
6                   | 285                                   | 391
7                   | 282                                   | 378
8                   | 284                                   | 384
9                   | 289                                   | 370
10                  | 283                                   | 391
**Average**         | **286.8**                             | **380.7**
**Avg. per ore**    | **4.48**                              | **5.95**



#### Lapis Lazuli Ore

Run                 | Total Drops (Regular Pickaxe) (Set 3) | Total Drops (Fortune III) (Set 4)
-:                  | -:                                    | -:
1                   | 398                                   | 799
2                   | 383                                   | 903
3                   | 390                                   | 894
4                   | 395                                   | 783
5                   | 371                                   | 840
6                   | 400                                   | 847
7                   | 379                                   | 870
8                   | 387                                   | 944
9                   | 392                                   | 882
10                  | 396                                   | 1013
**Average**         | **389.1**                             | **877.5**
**Avg. per ore**    | **6.08**                              | **13.71**

## Conclusion

The results of our tests showed that our hypothesis regarding redstone drop rates was pretty accurate, but only accurate for regular drops for lapis lazuli. The results from the Fortune III’s effects on lapis lazuli ore deviate from our hypothesis quite a bit, and even though it may first seem like this deviation ranges wide enough to be labeled an outlier, the actual range is 12.48 (the min, 799) to 15.83 (the max, 1013). We can therefore say that it is more likely than not that our hypothesis is wrong.

Our data is pretty conclusive for the first three sets, due to their limited range of deviation, but more testing may be needed for set 4.

## Sources

1. *[Redstone](http://minecraft.gamepedia.com/Redstone),* minecraft.gamepedia.com, published 2016-09-17, accessed 2016-09-22.
2. *[Lapis Lazuli](http://minecraft.gamepedia.com/Lapis_Lazuli),* minecraft.gamepedia.com, published 2016-09-22, accessed 2016-09-22.
