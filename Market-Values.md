# Market Values

## Ores

### Blocks

**Notes:** The *Base Value* column is inaccurate due to vein sizes. The *Adjusted for Vein Sizes* column adjusts these values for their respective average vein sizes. [[1]](http://www.minecraftforum.net/forums/minecraft-discussion/discussion/2207780) The function for accomplishing this is:

    y = ( baseValue / avgVeinSize ) * goldVeinSize

    baseValue    = from ‘Base Value’ column
    avgVeinSize  = average vein size of respective ore
    goldVeinSize = average vein size of gold

Item                    | Base Value    | Adjusted for Vein Sizes   | Notes
:-                      | -:            | -:                        | :-
Coal Ore `C`            | 7.82          | 2.39                      | avg. 22.03 blocks/vein
Iron Ore `C`            | 12.91         | 12.91                     | same vein size as gold
Gold Ore `C`            | 9.00          | 9.00                      | no change from base, avg. 6.82 blocks/vein
Redstone Ore `C`        | 9.71          | 11.68                     | avg. 5.67 blocks/vein
Lapis Lazuli Ore `C`    | 145.61        | 202.25                    | avg. 4.91 blocks/vein
Emerald Ore `C`         | 159.39        | 1,087.04                  | 1 block/vein
Diamond Ore `C`         | 75.90         | 91.29                     | avg. 5.67 blocks/vein

### Drops (Fortune III)

**Notes:** These values are based on those in the *Adjusted for Vein Sizes* column in the previous section and are further adjusted for the Fortune III enchantment.

Item            | Ore Value | Multiplier    | Adjusted Value    | Notes
:-              | -:        | -:            | -:                | :-
Coal            | 2.39      | 0.4545        | 1.09              | 120% drop increase
Redstone Dust   | 11.68     | 0.75          | 8.76              | 33% drop increase [[2]](https://bitbucket.org/Nurdoidz/annwn/src/a96bedbcee1c4a628a28078c9d4f67dda0acd313/Experiments/RedstoneLapisDropRatesFortune3.md)
Lapis Lazuli    | 202.25    | 0.4545        | 91.92             | 120% drop increase
Emerald         | 1,087.04  | 0.4545        | 494.06            | 120% drop increase
Diamond         | 91.29     | 0.4545        | 41.49             | 120% drop increase

## Terrain

Item    | Value
:-      | -:
Stone   |

# Sources

1. *“[How ‘spawn size’ correlates to actual vein size of ores](http://www.minecraftforum.net/forums/minecraft-discussion/discussion/2207780)”,* TheMasterCarver, minecraftforum.net, published 2014-09-06, accessed 2016-09-22.
2. *“[Redstone and Lapis Lazuli Ore Drop Rates and Fortune III’s Effect](https://bitbucket.org/Nurdoidz/annwn/src/a96bedbcee1c4a628a28078c9d4f67dda0acd313/Experiments/RedstoneLapisDropRatesFortune3.md)”,* Nurdoidz, bitbucket.org, published 2016-09-22, accessed 2016-09-22.
